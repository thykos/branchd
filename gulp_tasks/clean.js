import gulp from 'gulp';
import clean from 'gulp-clean';
import notifier from 'node-notifier';
import {Public} from './dist';

export default gulp.task('clean', () =>
  gulp.src(Public, {read: false})
    .pipe(clean({force: true}))
    .pipe(notifier.notify({
      title: 'Build',
      icon: 'info',
      message: 'Public is cleared'
    }))
);
