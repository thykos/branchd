import gulp from 'gulp';
import sourcemaps from 'gulp-sourcemaps';
import babel from 'gulp-babel';
import concat from 'gulp-concat';
import {Scripts, PublicScripts} from './dist';
import plumber from 'gulp-plumber';
import errorsHandler from './error';
import {reload} from './server';


gulp.task('compileScripts', () =>
  gulp.src(Scripts)
    .pipe(plumber({errorHandler: errorsHandler('Scripts')}))
    .pipe(sourcemaps.init())
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(concat('app.js'))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(PublicScripts))
    .pipe(reload())
);

export default gulp.task('scripts', ['compileScripts'], () =>
  gulp.watch(Scripts, ['compileScripts'])
);

export const scriptsProd  = gulp.task('scriptsProd', ['compileScripts']);
