import gulp from 'gulp';
import newer from 'gulp-newer';
import {Images, PublicImages} from './dist';

gulp.task('copy:images', function () {
  return gulp.src(Images)
    .pipe(newer(Images))
    .pipe(gulp.dest(PublicImages))
});

export default gulp.task('images', ['copy:images'], () =>
  gulp.watch(Images, ['copy:images'])
);

export const imagesProd = gulp.task('imagesProd', ['copy:images']);
