$(document).ready(() => {
 $('.animated-js').viewportChecker({
   classToAdd: 'start-animation',
   offset: -100,
   repeat: false
 });

  $('.scroll-to').on('click', function () {
    event.preventDefault();
    var id = $(this).attr('href'),
      top = $(id).offset().top;
    $('body,html').animate({scrollTop: top}, 500);
  });

  if (window.location.hash && window.location.hash.includes('#feedback')) {
    const notification = $('.b-notification');
    notification.removeClass('hidden');
    const t = setTimeout(() => {
      notification.removeClass('fadeInDown').addClass('fadeOutUp');
      clearTimeout(t);
    }, 3000);


  }
});
